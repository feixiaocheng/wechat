from __future__ import unicode_literals
import requests
import itchat
import time


def get_news():
    url = "http://open.iciba.com/dsapi"
    r = requests.get(url)
    contents = r.json()['content']
    translation = r.json()['translation']
    return contents, translation


def send_news():
    try:
        itchat.auto_login(hotReload=True)

        my_friend = itchat.search_friends(name='zechen')
        XiaoMing = my_friend[0]["UserName"]

        message1 = str(get_news()[0])
        content = str(get_news()[1][17:])
        message2 = str(content)
        message3 = "to lover"

        itchat.send(message1, toUserName=XiaoMing)
        itchat.send(message2, toUserName=XiaoMing)
        itchat.send(message3, toUserName=XiaoMing)
    except:
        message4 = 'bug'
        print message4
        itchat.send(message4, toUserName=XiaoMing)


def main():
    send_news()


if __name__ == '__main__':
    main()